﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWall : MonoBehaviour
{
	private Renderer _renderer;
	private Rigidbody _rigidbody;
	private Collider _collider;
	private string _liftstate;

	public Material OriginalObjectMaterial;
	public string MenuLabel;
	public GameObject LiftPlatform;
	public GameObject Floor;
	public float Speed;
	public float LiftDistance;
	// Use this for initialization
	void Start ()
	{
		// When the object is created, lets get everything we need to know about!
		_renderer = gameObject.GetComponent<Renderer>();
		_rigidbody = gameObject.GetComponent<Rigidbody>();
		_collider = gameObject.GetComponent<Collider>();
	}

	public void OnEnter()
	{
		// When the pointer has entered, Make it red
		_renderer.material.color = Color.red;
	}

	public void OnExit()
	{
		// Once the pointer has exited the hitbox, let us reapply the previous material
		_renderer.material = OriginalObjectMaterial;
	}

	public void OnClickStart()
	{
		// When the pointer has entered, Make it red
		_renderer.material.color = Color.blue;
	}

	public void OnClickEnd()
	{
		_BehaviourSelector();
	}

	public void Update()
	{
		_MoveToBeacon();
	}

	private void _BehaviourSelector()
	{
		switch(MenuLabel)
		{
			case "Lift Down":
				_LiftDestinationMove(true);
				break;
			case "Lift Up":
				_LiftDestinationMove(false);
				break;
			case "Lights":
				_Lights();
				break;
		}
	}
	private void _LiftDestinationMove(bool isDown)
	{
		float _increment = LiftDistance;
		if (isDown)
		{
			_increment = -_increment;
		}
		Floor.transform.position += new Vector3(0,_increment,0);
	}
	private void _Lights()
	{
		_renderer.material.color = Color.white;
		_liftstate = "Toggle Lights";
	}

	private void _MoveToBeacon()
	{
		// The step size is equal to speed times frame time.
		float _step = Speed * Time.deltaTime;
		LiftPlatform.transform.position = Vector3.MoveTowards(LiftPlatform.transform.position,
															  Floor.transform.position,
															  _step);
	}
}
