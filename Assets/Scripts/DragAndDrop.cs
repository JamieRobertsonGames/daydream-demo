﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour {

	private Renderer _renderer;
	private Rigidbody _rigidbody;
	private Collider _collider;

	public Material OriginalObjectMaterial;
	public bool IsRenderer = true;
	// Use this for initialization
	void Start ()
	{
		// When the object is created, lets get everything we need to know about!
		if (IsRenderer)
		{
			_renderer = gameObject.GetComponent<Renderer>();
		}
		_rigidbody = gameObject.GetComponent<Rigidbody>();
		_collider = gameObject.GetComponent<Collider>();
	}

	public void OnEnter()
	{
		// When the pointer has entered, Make it red
		if (IsRenderer)
		{
			_renderer.material.color = Color.cyan;
		}
	}

	public void OnExit()
	{
		// Once the pointer has exited the hitbox, let us reapply the previous material
		if (IsRenderer)
		{
			_renderer.material = OriginalObjectMaterial;
		}
	}

	public void OnGrab()
	{
		// Disable the gravity to make sure it doesn't interfere
		_rigidbody.useGravity = false;
		_rigidbody.isKinematic = true;

		// This is the transform of the pointer hit
		Transform _pointerTransform = GvrPointerInputModule.Pointer.PointerTransform;
		this.transform.SetParent(_pointerTransform, true);
	}

	public void OnRelease()
	{
		this.transform.SetParent(null, true);

		// Old object gets gravity back too, with its collider
		_rigidbody.useGravity = true;
		_rigidbody.isKinematic = false;
	}
}
