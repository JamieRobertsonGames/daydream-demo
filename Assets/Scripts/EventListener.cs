﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventListener : MonoBehaviour
{
	private Renderer _renderer;
	private Rigidbody _rigidbody;
	private Collider _collider;

	private Rigidbody _new_rigidbody;
	public GameObject CloneObject;
	public GameObject newGameObject;
	public Material OriginalObjectMaterial;
	// Use this for initialization
	void Start ()
	{
		// When the object is created, lets get everything we need to know about!
		_renderer = gameObject.GetComponent<Renderer>();
		_rigidbody = gameObject.GetComponent<Rigidbody>();
		_collider = gameObject.GetComponent<Collider>();
	}

	public void OnEnter()
	{
		// When the pointer has entered, Make it red
		_renderer.material.color = Color.red;
	}

	public void OnExit()
	{
		// Once the pointer has exited the hitbox, let us reapply the previous material
		_renderer.material = OriginalObjectMaterial;
	}

	public void OnGrab()
	{
		/*
		So we're going to have to do a few things when picking up an object...

		Clone the object we're grabbing. While doing that, we will get the target object to:
				- Disable the collider
				- Stop using gravity
		This is to avoid the object interfearing with the new clone with ~physics~
		 */
		newGameObject = Instantiate(CloneObject);
		_collider.enabled = false;
		_rigidbody.useGravity = false;

		//... For the same reason, let us make the object Kinematic and not use gravity while we're dragging
		_new_rigidbody = newGameObject.GetComponent<Rigidbody>();
		_new_rigidbody.isKinematic = true;
		_new_rigidbody.useGravity = false;

		// Spawn the clone object where the pointer is
		newGameObject.transform.position = GvrPointerInputModule.Pointer.PointerIntersection +
											new Vector3(0,0,0.5f);

		// This is the transform of the pointer hit
		Transform pointerTransform = GvrPointerInputModule.Pointer.PointerTransform;
		newGameObject.transform.SetParent(pointerTransform, true);
	}

	public void OnRelease()
	{
		// Once we release the object..
		// .. Remove the parent to the pointer
		newGameObject.transform.SetParent(null, true);

		// Gravity is back on the new object
		_new_rigidbody.useGravity =true;
		_new_rigidbody.isKinematic = false;

		// Old object gets gravity back too, with its collider
		_rigidbody.useGravity = true;
		_collider.enabled = true;
	}
}
