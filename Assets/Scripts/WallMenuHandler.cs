﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMenuHandler : MonoBehaviour 
{
	public List<GameObject> MenuItems;
	private List<Renderer> _renderer;
	private List<Rigidbody> _rigidbody;
	private List<Collider> _collider;
	// Use this for initialization
	void Start () 
	{
		foreach (var item in MenuItems) 
		{			// When the object is created, lets get everything we need to know about!
			_renderer.Add(gameObject.GetComponent<Renderer>());
			_rigidbody.Add(gameObject.GetComponent<Rigidbody>());
			_collider.Add(gameObject.GetComponent<Collider>());
		}
	}

	void SelectMenuItem()
	{
		
	}
}
