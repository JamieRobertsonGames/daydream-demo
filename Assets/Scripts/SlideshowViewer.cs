﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideshowViewer : MonoBehaviour
{
	private Renderer _renderer;
	private int _slidenumber;
	public List<Material> Slides;
	// Use this for initialization
	void Start ()
	{
		_renderer = this.GetComponent<Renderer>();
		_slidenumber = 0;
		_renderer.material = Slides[0];
	}

	public void SwapSlides()
	{
		_slidenumber += 1;
		if (_slidenumber < Slides.Count)
		{
			_renderer.material = Slides[_slidenumber];
		}
		else
		{
			_slidenumber = 0;
			_renderer.material = Slides[_slidenumber];
		}
	}
}
